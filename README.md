<h1>How to Create RecyclerView?</h1>

**Steps to create RecyclerView:**
1. Design placeholder layout
1. ViewHolder class
1. Adapter class
1. Activity class

**Advantages:**
* Reduce code complexity
* Reuse recycler view layout
