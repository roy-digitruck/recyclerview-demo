package com.example.androidrecyclerview

import android.view.View
import com.example.androidrecyclerview.recycler.BaseModel
import com.example.androidrecyclerview.recycler.RViewHolder
import kotlinx.android.synthetic.main.list_item.view.*

class HobbyViewHolder(itemView: View) : RViewHolder(itemView) {
    override fun setData(data: BaseModel?, pos: Int) {
        val hobby = data as Hobby
        itemView.txvTitle.text = hobby!!.title
    }
}
