package com.example.androidrecyclerview

import com.example.androidrecyclerview.recycler.BaseModel

data class Hobby(var title: String) : BaseModel

object Supplier {
    val hobbies = listOf(
        Hobby("Swimming"),
        Hobby("Reading"),
        Hobby("Programming"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Swimming"),
        Hobby("Reading"),
        Hobby("Programming"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Swimming"),
        Hobby("Reading"),
        Hobby("Programming"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Swimming"),
        Hobby("Reading"),
        Hobby("Programming"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Sleeping"),
        Hobby("Walking"),
        Hobby("Reading"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Swimming"),
        Hobby("Reading"),
        Hobby("Programming"),
        Hobby("Computer Gaming"),
        Hobby("Playing"),
        Hobby("Sleeping"),
        Hobby("Walking"),
        Hobby("Reading")
    )
}