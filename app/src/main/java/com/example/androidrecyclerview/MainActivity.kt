package com.example.androidrecyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidrecyclerview.recycler.LayoutManagerProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LayoutManagerProvider.of(this)
        recyclerView.adapter = HobbiesAdapter(this, Supplier.hobbies)
    }
}
