package com.example.androidrecyclerview

import android.content.Context
import android.view.View
import com.example.androidrecyclerview.recycler.BaseModel
import com.example.androidrecyclerview.recycler.RAdapter

class HobbiesAdapter(context: Context, dataList: List<BaseModel>) :
    RAdapter<HobbyViewHolder>(context, dataList) {

    override fun getLayout(): Int = R.layout.list_item

    override fun getViewHolder(view: View): HobbyViewHolder {
        return HobbyViewHolder(view)
    }
}
