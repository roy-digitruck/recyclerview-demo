package com.example.androidrecyclerview.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class RViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun setData(data: BaseModel?, pos: Int)
}
