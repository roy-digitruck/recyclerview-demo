package com.example.androidrecyclerview.recycler

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LayoutManagerProvider {

    companion object {
        fun of(context: Context): LinearLayoutManager {
            val layoutManager = LinearLayoutManager(context)
            layoutManager.orientation = RecyclerView.VERTICAL
            return layoutManager
        }
    }
}