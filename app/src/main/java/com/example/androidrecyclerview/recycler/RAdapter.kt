package com.example.androidrecyclerview.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.io.Serializable

abstract class RAdapter<R : RViewHolder>(private val context: Context, private val dataList: List<BaseModel>) :
    RecyclerView.Adapter<R>() {

    abstract fun getLayout(): Int

    abstract fun getViewHolder(view: View): R

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): R {
        val view = LayoutInflater.from(context).inflate(this.getLayout(), parent, false)
        return this.getViewHolder(view)
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: R, position: Int) {
        val hobby = dataList[position]
        holder.setData(hobby, position)
    }
}
